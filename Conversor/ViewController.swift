//
//  ViewController.swift
//  Conversor
//
//  Created by Otto Colomina Pardo on 20/10/17.
//  Copyright © 2017 Universidad de Alicante. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    let conversor = ConversorEUR_USD()
    
    //propiedad del view controller
    var observador : NSKeyValueObservation!
    
    @IBOutlet weak var dolares: UITextField!
    @IBOutlet weak var euros: UITextField!
    
    @IBOutlet weak var tipoCambioLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        observador = conversor.observe(\.unEURenUSD) { obj, cambio in
            //self.tipoCambioLabel.text = String(format: "1 € = %.2f $", obj.unEURenUSD)
            let e = Emisor()
            let r = Receptor()
            r.suscribirse(receiver:self.tipoCambioLabel)
            e.enviar(cantidad:obj.unEURenUSD)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func calcularUSDPulsado(_ sender: Any) {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        if let numEuros = nf.number(from: self.euros.text!) {
            let euros = Float(truncating:numEuros)
            let dolares = conversor.cuantosUSDson(eur: euros)
            self.dolares.text = String(dolares)
        }
    }
    
   
    @IBAction func calcularEURPulsado(_ sender: Any) {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        if let numDolares = nf.number(from: self.dolares.text!) {
            let dolares = Float(truncating:numDolares)
            let euros = conversor.cuantosEURson(usd: dolares)
            self.euros.text = String(euros)
        }
    }
    
    // Notificaciones
    class Emisor {
        func enviar(cantidad:Float) {
            //Obtenemos el centro de notificaciones por defecto
            //Las notificaciones tienen un nombre, un objeto que las envía (si lo ponemos a nil no queda constancia de quién) y datos adicionales, un diccionario con los datos que queramos
            let nc = NotificationCenter.default
            nc.post(name: NSNotification.Name(rawValue: "cantidad"), object: nil, userInfo: ["valor":1, "cantidad":cantidad])
        }
    }
    class Receptor {
        @IBOutlet weak var tipoCambioLabel: UILabel!
        func suscribirse(receiver:UILabel) {
            self.tipoCambioLabel = receiver
            let nc = NotificationCenter.default
            //primer parámetro: añadimos como observador a nosotros (self)
            //selector: al recibir la notificación se llama al método recibir
            //name: nombre de la notificación que nos interesa
            //object: objeto del que nos interesa recibir notificaciones. nil == cualquiera
            nc.addObserver(self, selector:#selector(self.recibir), name:NSNotification.Name(rawValue:"cantidad"), object: nil)
        }
        @objc func recibir(notificacion:Notification) {
            print("Notificacion recibida. Actualizando UI")
            if let userInfo = notificacion.userInfo {
                let cantidad = userInfo["cantidad"] as! Float
                tipoCambioLabel.text = String(format: "1 € = %.2f $", cantidad)
            }
        }
    }
    
}






