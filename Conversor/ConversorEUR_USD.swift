//
//  ConversorEUR_USD.swift
//  Conversor
//
//  Created by Otto Colomina Pardo on 20/10/17.
//  Copyright © 2017 Universidad de Alicante. All rights reserved.
//

import Foundation

class ConversorEUR_USD : NSObject{
    @objc dynamic var unEURenUSD : Float = 0.0
    
    override init() {
        super.init()
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) {
            timer in
            self.actualizarTipo()
        }
    }
    
    func cuantosUSDson(eur:Float)->Float {
        return eur*self.unEURenUSD
    }
    func cuantosEURson(usd:Float)->Float {
        return usd/self.unEURenUSD
    }
    func actualizarTipo() {
        //Esto se debería consultar a algún servidor.
        //Lo simulamos con un valor aleatorio
        self.unEURenUSD = Float((100.0+Double(arc4random()%50))/100.0);
        //print("Cambio actual: \(self.unEURenUSD)");
    }
}
